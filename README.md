# funathon_insee_environnement_2022

Espace projet de l'équipe DREAL Grand Est pour le Funathon organisé par l'Insee sur la thématique Environnement, les 21 et 22 juin 2022.

__Déplacé le 6 octobre 2022 dans  https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/funathon_insee_environnement_2022 -> https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/pub/funathon_insee_environnement_2022__

__A FAIRE: Une fois le feu vert de migration envoyé à la Dnum pour changer l'architecture de dépôt sur la Forge GitLab du MTE : Changer la visibilité Interne --> public__

## avant le funathon

**À faire de manière prioritaire :**

* se créer un compte sur le SSPCloud : [https://datalab.sspcloud.fr/home](https://datalab.sspcloud.fr/home)
* s'inscrire au slack pour recevoir les news : ssplab-funathon.slack.com
* installer l'application Zoom
* s'ouvrir un compte sur la forge gitlab MTE du ministère, et demander à être ajouter aux membres de l'espace public SCDD : [https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/scdd/funathon_insee_environnement_2022](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/dreal-grandest)
* tester sa connexion au projet [funathon2022](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/dreal-grandest/funathon_insee_environnement_2022) de la forge gitlab via Rstudio ou Jupyter. Pour se faire, se référer au [guide d'utilisation de la forte gitlab SCDD](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-grandest/guide.scdd.gitlab)

**À faire éventuellement :**

* participer à la présentation Git et Datalab du 16 juin après-midi
* se familiariser avec l'accès à l'espace de stockage aws du Datalab : voir notamment le [projet de l'année dernière](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/dreal-grandest/funathon_insee_airbnb_2021/-/issues/2)
* choisir un sujet (approche thématique ou outil)

Sujet | Thématique | Outils
------|------------|------
1|transition écologique|NLP
2|changement climatique|elasticsearch,API
3|rénovation énergétique|API,NLP,dataviz
4|installations polluantes|elasticsearch
5|qualité de l'air|ML
6|montée des eaux|appariements spatiaux
7|Grand Débat National|NLP
8|occupation des sols|Quarto

* choisir son approche : découverte ou projet
* choisir son mode de travail : présentiel (en petit groupe) ou distanciel
* partage des données : utilisation de l'entrepot postgres ? Si oui, schema w_scdd ou un schema ad hoc ?
